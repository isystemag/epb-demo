# Electric Parking Brake Demonstration Platform #

This project started life as the Diploma Thesis for Markus Huber of the
University of Applied Sciences in Munich. The aim of the project was to consider
the Electric Parking Brake (EPB) application that is slowly replacing the 
traditional mechanical handbrake in vehicles today. The application was 
analysed against the ISO 26262 specification with respect to risk. The goal of 
the project was simply to implement the input rocker switch that applies and 
releases the brake, as well as any further hardware and software to demonstrate
the application. This is the outcome:

![EPB Demonstrator](/images/epb-and-stuart-480x423.png)

This repository is still a work in progress! Please take a look around and, if
you need some help, contact us using the details below. 

(last README update 25th Oct 2016)

### What is in this repository? ###

The repository consists of several elements, including:

* Source code for the EPB application (consisting of a master and slave MCU)
* UML test model for testing the entire system
* UML test model example for unit testing
* testIDEA test specification example for unit testing

The repository structure is as follows:

* documentation - Project documentation in Word format (not complete)
    * doxygen - Doxygen file needed to create source code documentation
* hw - Folder for nanoHIL hardware designs
    * epb-cable-loom - PCB designed to implement failures in cable-loom
    * epb-switch - PCB design to implement HIL for control inputs (brake 
    rocker-switch, ignition switch and motion switch)
* images - various images relevant to the project
* mbt - Test models for nanoHIL test case generation
    * UML - Enterprise Architect models for testing purposes
    * python_nanoHIL - templates required for MBTsuite Python script generation
* nanoHIL - Python scripts used for system testing of nanoHIL
* sw - Embedded software code for the project
    * master-mcu - Arduino code for Arduino DUE on master MCU board
    * slace-mcu - Arduino code for Arduino M0 Pro on slave MCU board

### What software will I need to use this code? ###

Various applications were used in the development of this demonstrator:

* winIDEA - the embedded software development IDE from 
[iSYSTEM](http://www.isystem.com/products/software/winidea)
* testIDEA - the unit testing Original Binary Code (OBC) tool included in 
winIDEA from iSYSTEM
* Enterprise Architect (EA) - UML modelling and design tool from 
[SparxSystems](http://www.sparxsystems.com/)
* MBTsuite - Test case generator from 
[sepp.med GmbH](http://www.seppmed.de/produkte/mbtsuite.html)
* MBTassist - EA plug-in designed to support test model development from 
sepp.med GmbH
* DesignSpark PCB - used to design the boards needed for the nanoHIL 
implementation 
[RS Components](https://www.rs-online.com/designspark/pcb-software)
* [Arduino](http://www.arduino.org/) - The code was developed for the Arduino 
DUE and Arduino M0 Pro thanks to the ease of rapid prototyping on these devices

### Who do I talk to? ###

If you have any questions please contact Stuart Cording at iSYSTEM AG via email
at inews@isystem.com, or by phone on +49 8138 6971 59.
