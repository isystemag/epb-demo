
def __init__():
    pass

class TestStatus:

    STATUS_OK = 'OK'
    STATUS_FAIL = 'FAIL'
    STATUS_ERROR = 'ERROR'
    
    def __init__(self, status, message):
        self.status = status
        self.message = message

        
    def getStatus(self):
        return self.status

    
    def getMessage(self):
        return self.message

    def __str__(self):
        return self.status + " " + self.message
