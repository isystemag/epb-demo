# This script was generated with SeppMed MBTsuite tool. Do not modify it manually!

from __future__ import print_function

import sys
sys.path.append('../../nanoHIL')
import time
import isystem.connect as ic
import teststatus as ts
import nanohil as nh
from isystem.connect import IConnectDebug


#EPB    - TestCase_1
# 
def EPB(results):
    print("EPB - TestCase_1")
    key = 'EPB'
    results[key] = []
    testStatus = "PASS"

    try:
        print('TS: Init nanoHIL')
        nh.initNanoHil()
        nh.resetAndRun()
                 
        # Put EPB through post-installation init sequence
        nh.turnIgnitionOn(1)
        time.sleep(2)
        nh.turnIgnitionOff(1)
        time.sleep(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: VM - Vehicle is moving')
        nh.vehicleInMotion(0)

        print("TS: VM - Note that Brake State LED = OFF")

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 2 second')
        time.sleep(2)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: FICL - SIG1 Short To Vcc')
        nh.applyCableShort1(1)
        nh.removeCableShort1(1)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VM - Vehicle is moving')
        nh.vehicleInMotion(0)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: nanoHIL Shutdown')
        nh.deinitSystem()

    except Exception as ex:
        testStatus = "FAIL"
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_ERROR, "EPB(): " + str(ex)))

    if testStatus == "PASS":
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_OK, "EPB(): " + "!PASSED!"))
    else:
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): " + "#FAILED#"))


    print("")
    return


#EPB0002    - TestCase_2
# 
def EPB0002(results):
    print("EPB0002 - TestCase_2")
    key = 'EPB0002'
    results[key] = []
    testStatus = "PASS"

    try:
        print('TS: Init nanoHIL')
        nh.initNanoHil()
        nh.resetAndRun()
                 
        # Put EPB through post-installation init sequence
        nh.turnIgnitionOn(1)
        time.sleep(2)
        nh.turnIgnitionOff(1)
        time.sleep(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print("TS: AB - Note the Brake State LED = ON")
         

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: FICL - Cable Break In SIG2')
        nh.applyCableBreak2(1)
        nh.removeCableBreak2(1)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: nanoHIL Shutdown')
        nh.deinitSystem()

    except Exception as ex:
        testStatus = "FAIL"
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_ERROR, "EPB0002(): " + str(ex)))

    if testStatus == "PASS":
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_OK, "EPB0002(): " + "!PASSED!"))
    else:
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB0002(): " + "#FAILED#"))


    print("")
    return


#EPB0003    - TestCase_3
# 
def EPB0003(results):
    print("EPB0003 - TestCase_3")
    key = 'EPB0003'
    results[key] = []
    testStatus = "PASS"

    try:
        print('TS: Init nanoHIL')
        nh.initNanoHil()
        nh.resetAndRun()
                 
        # Put EPB through post-installation init sequence
        nh.turnIgnitionOn(1)
        time.sleep(2)
        nh.turnIgnitionOff(1)
        time.sleep(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: VM - Vehicle is moving')
        nh.vehicleInMotion(0)

        print("TS: VM - Note that Brake State LED = OFF")

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 4 second')
        time.sleep(4)

        print("TS: AB - Note the Brake State LED = ON")
         

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: AB - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: FICL - SIG2 Short To Vcc')
        nh.applyCableShort2(1)
        nh.removeCableShort2(1)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: nanoHIL Shutdown')
        nh.deinitSystem()

    except Exception as ex:
        testStatus = "FAIL"
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_ERROR, "EPB0003(): " + str(ex)))

    if testStatus == "PASS":
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_OK, "EPB0003(): " + "!PASSED!"))
    else:
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB0003(): " + "#FAILED#"))


    print("")
    return


#EPB0004    - TestCase_4
# 
def EPB0004(results):
    print("EPB0004 - TestCase_4")
    key = 'EPB0004'
    results[key] = []
    testStatus = "PASS"

    try:
        print('TS: Init nanoHIL')
        nh.initNanoHil()
        nh.resetAndRun()
                 
        # Put EPB through post-installation init sequence
        nh.turnIgnitionOn(1)
        time.sleep(2)
        nh.turnIgnitionOff(1)
        time.sleep(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print("TS: RB - Note that Brake State LED = OFF")

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print("TS: RB - Note that Brake State LED = OFF")

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('TS: FICL - Cable Break In Vcc')
        nh.applyCableBreak1(1)
        nh.removeCableBreak1(1)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VM - Vehicle is moving')
        nh.vehicleInMotion(0)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 2 second')
        time.sleep(2)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VM - Vehicle is moving')
        nh.vehicleInMotion(0)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 4 second')
        time.sleep(4)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VM - Vehicle is moving')
        nh.vehicleInMotion(0)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VM - Vehicle is moving')
        nh.vehicleInMotion(0)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 4 second')
        time.sleep(4)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: nanoHIL Shutdown')
        nh.deinitSystem()

    except Exception as ex:
        testStatus = "FAIL"
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_ERROR, "EPB0004(): " + str(ex)))

    if testStatus == "PASS":
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_OK, "EPB0004(): " + "!PASSED!"))
    else:
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB0004(): " + "#FAILED#"))


    print("")
    return


#EPB0005    - TestCase_5
# 
def EPB0005(results):
    print("EPB0005 - TestCase_5")
    key = 'EPB0005'
    results[key] = []
    testStatus = "PASS"

    try:
        print('TS: Init nanoHIL')
        nh.initNanoHil()
        nh.resetAndRun()
                 
        # Put EPB through post-installation init sequence
        nh.turnIgnitionOn(1)
        time.sleep(2)
        nh.turnIgnitionOff(1)
        time.sleep(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print("TS: RB - Note that Brake State LED = OFF")

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: FICL - SIG1 Short To Vcc')
        nh.applyCableShort1(1)
        nh.removeCableShort1(1)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VM - Vehicle is moving')
        nh.vehicleInMotion(0)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 4 second')
        time.sleep(4)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: nanoHIL Shutdown')
        nh.deinitSystem()

    except Exception as ex:
        testStatus = "FAIL"
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_ERROR, "EPB0005(): " + str(ex)))

    if testStatus == "PASS":
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_OK, "EPB0005(): " + "!PASSED!"))
    else:
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB0005(): " + "#FAILED#"))


    print("")
    return


#EPB0006    - TestCase_6
# 
def EPB0006(results):
    print("EPB0006 - TestCase_6")
    key = 'EPB0006'
    results[key] = []
    testStatus = "PASS"

    try:
        print('TS: Init nanoHIL')
        nh.initNanoHil()
        nh.resetAndRun()
                 
        # Put EPB through post-installation init sequence
        nh.turnIgnitionOn(1)
        time.sleep(2)
        nh.turnIgnitionOff(1)
        time.sleep(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: FICL - SIG2 Short To Vcc')
        nh.applyCableShort2(1)
        nh.removeCableShort2(1)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VM - Vehicle is moving')
        nh.vehicleInMotion(0)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 2 second')
        time.sleep(2)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VM - Vehicle is moving')
        nh.vehicleInMotion(0)

        print('TS: VM - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: AB - Pressing Apply Brake')
        nh.pressApplyBrake(0)

        print('TS: AB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: AB - Releasing Apply Brake')
        nh.releaseApplyBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: VS - Vehicle is stationary')
        nh.vehicleHalted(1)

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: RB - Press Release Brake button')
        nh.pressReleaseBrake(0)

        print('TS: RB - Wait 1 second')
        time.sleep(1)

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('TS: RB - Releasing Release Brake')
        nh.releaseReleaseBrake(1)

        print('TS: IOn - Ignition On')
        nh.turnIgnitionOn(0)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED ON')
        vpResult = nh.verifyBrakeLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - ON - failed"))

        print('TS: IOn - Wait 2 seconds')
        time.sleep(2)

        print('VP: Brake Warning LED ON')
        vpResult = nh.verifyWarningLight("ON")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - ON - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: IOff - Ignition Off')
        nh.turnIgnitionOff(1)

        print('VP: Brake Warning LED OFF')
        vpResult = nh.verifyWarningLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyWarningLight - OFF - failed"))

        print('VP: Brake State LED OFF')
        vpResult = nh.verifyBrakeLight("OFF")
         
        if vpResult == False:
            testStatus = "FAIL"
            nh.testHasFailed()
            results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB(): Ignition On: " + "verifyBrakeLight - OFF - failed"))

        print('TS: nanoHIL Shutdown')
        nh.deinitSystem()

    except Exception as ex:
        testStatus = "FAIL"
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_ERROR, "EPB0006(): " + str(ex)))

    if testStatus == "PASS":
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_OK, "EPB0006(): " + "!PASSED!"))
    else:
        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "EPB0006(): " + "#FAILED#"))


    print("")
    return



def executeAllTests():

    results = {}
    nh.initSystem()

    # Initialise HIL system 
    #nh.initSystem()
    #nh.initNanoHil()

    # Download code 
    #try:
    #    dbgCtrl.download() # downloads appplication to target
    #    dbgCtrl.run()
    #    #dbgCtrl.waitUntilStopped()
    #except Exception as ex:
    #    print(ex)

    waitTime = nh.initStartDelay()
    if waitTime > 0:
        time.sleep(waitTime)


    EPB(results)
    EPB0002(results)
    EPB0003(results)
    EPB0004(results)
    EPB0005(results)
    EPB0006(results)

    nh.createReport(results)

    print()
    nh.getOverallTestStatus()

if __name__ == '__main__':
    executeAllTests()


