
# This module should be implemented by the user, who knows the meaning of
# target I/O signals and variables. Basically there are two possibilities
# with iSystem tools:
# - read/write I/O signals with iSystem IO module attached on BlueBox
# - read/write variables on target and stub I/O functions on target and
#   define their return values.

# (c) iSYSTEM AG, Mar. 2016

# Configured for EPB-Demo project

from __future__ import print_function

import time
import isystem.connect as ic
import teststatus as ts

g_debug = None
g_dataCtrl = None
g_hilCtrl = None
#_exit_testing = False
#g_testStatus = None

# This stores the state of the brake signal light for precondition situations 
# during testing
#g_brakeSignalLightState = ''
# This stores the state of the brake warning light for precondition situations 
# during testing
#g_brakeWarningLightState = ''
# This stores the state of the entire test process
#g_testStatus = ''

class overallTestStatus:
    """
    Keeps track of overall status of all tests
    """
    
    def __init__(self):
        self.testStatus = '## nano-HIL TESTS - PASSED ##'
        self.failureCounter = 0
        
    def outputTestStatus(self):
        print(self.testStatus)
        
    def testFailed(self):
        self.testStatus = '!!nano-HIL TESTS - FAILED!!'
        self.failureCounter += 1
        
    def outputFailureCounter(self):
        print(self.failureCounter)

class ledState:
    """
    Keeps track of status of LEDs
    """

    def __init__(self):
        self.ledStatus = 'OFF'
        
    def ledOn(self):
        self.ledStatus = 'ON'
        
    def ledOff(self):
        self.ledStatus = 'OFF'
        
    def resetLedStatus(self):
        self.ledStatus = 'OFF'
        
    def getLedStatus(self):
        return self.ledStatus


        
        
def initSystem():
    """
    Initializes isystem.connect and nano-HIL system
    """
    global g_debug, g_dataCtrl, g_hilCtrl
    #global g_brakeSignalLightState, g_brakeWarningLightState
    global testStatus, brakeIndicatorLedState, brakeErrorLedState
    
    global _exit_testing
    
    print('isystem.connect version: ' + ic.getModuleVersion())

    cmgr = ic.ConnectionMgr()
    cmgr.connectMRU('')

    g_debug = ic.CDebugFacade(cmgr)

    cpuStatus = g_debug.getCPUStatus()

    print('Downloading binary...')
    g_debug.download()
    
    print('Resetting target...')
    g_debug.reset()
    
    g_dataCtrl = ic.CDataController(cmgr)
    g_hilCtrl = ic.CHILController(cmgr)
    
    #g_testStatus = '## nano-HIL TESTS - PASSED ##'
    #g_brakeSignalLightState = 'OFF'
    #g_brakeWarningLightState = 'OFF'
    
    print('Resetting overall test status...')
    testStatus = overallTestStatus()
    
    print('Resetting brake indicator LED state...')
    brakeIndicatorLedState = ledState()
    #print(brakeIndicatorLedState.getLedStatus())
    
    print('Resetting brake error LED state...')
    brakeErrorLedState = ledState()
    #print(brakeErrorLedState.getLedStatus())
    
    _exit_testing = False


def resetAndRun():
    print('Reset and run.')
    g_debug.reset()
    g_debug.run()
    #print(g_testStatus)
    
    print('Clearing LED states.')
    brakeIndicatorLedState.resetLedStatus()
    #print(brakeIndicatorLedState.getLedStatus())
    brakeErrorLedState.resetLedStatus()
    #print(brakeErrorLedState.getLedStatus())

    
def deinitSystem():
    """
    Disconnect from nano-HIL system
    """
    print('Test case finished')
    
    g_debug.stop()
    
    initNanoHil()
    #print(g_testStatus)

def initNanoHil():
    # reset inputs in target
    g_hilCtrl.write('DOUT.DOUT0: LOW')
    g_hilCtrl.write('DOUT.DOUT1: LOW')
    g_hilCtrl.write('DOUT.DOUT2: LOW')
    g_hilCtrl.write('DOUT.DOUT3: LOW')
    g_hilCtrl.write('DOUT.DOUT4: LOW')
    g_hilCtrl.write('DOUT.DOUT5: LOW')
    g_hilCtrl.write('DOUT.DOUT6: LOW')
    g_hilCtrl.write('DOUT.DOUT7: LOW')
    
    time.sleep(1)
    
def initStartDelay():
    # user can define initial start-up delay here
    return 2

def testHasFailed():
    testStatus.testFailed()
    print('Failure detected - ', end="")
    testStatus.outputFailureCounter()
    
def getOverallTestStatus():
    testStatus.outputTestStatus()

# END OF PREDEFINED SECTION    

def turnIgnitionOn(delay):
    # turns ignition on
    g_hilCtrl.write('DOUT.DOUT5: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def turnIgnitionOff(delay):
    # turns ignition off
    g_hilCtrl.write('DOUT.DOUT5: LOW')
    if delay > 0:
        time.sleep(delay)

def vehicleInMotion(delay):
    # applies "In Motion" switch
    g_hilCtrl.write('DOUT.DOUT6: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def vehicleHalted(delay):
    # releases "In Motion" switch
    g_hilCtrl.write('DOUT.DOUT6: LOW')
    if delay > 0:
        time.sleep(delay)

def pressApplyBrake(delay):
    # presses "Apply Brake" switch
    g_hilCtrl.write('DOUT.DOUT4: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def releaseApplyBrake(delay):
    # releases "Apply Brake" switch
    g_hilCtrl.write('DOUT.DOUT4: LOW')
    if delay > 0:
        time.sleep(delay)

def pressReleaseBrake(delay):
    # presses "Release Brake" switch
    g_hilCtrl.write('DOUT.DOUT7: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def releaseReleaseBrake(delay):
    # releases "Release Brake" switch
    g_hilCtrl.write('DOUT.DOUT7: LOW')
    if delay > 0:
        time.sleep(delay)

def applyCableBreak1(delay):
    # applies cable loom break to Vcc
    g_hilCtrl.write('DOUT.DOUT1: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def removeCableBreak1(delay):
    # removes cable loom break to Vcc
    g_hilCtrl.write('DOUT.DOUT1: LOW')
    if delay > 0:
        time.sleep(delay)
    
def applyCableBreak2(delay):
    # applies cable loom break to SIG2
    g_hilCtrl.write('DOUT.DOUT2: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def removeCableBreak2(delay):
    # removes cable loom break to SIG2
    g_hilCtrl.write('DOUT.DOUT2: LOW')
    if delay > 0:
        time.sleep(delay)

def applyCableShort1(delay):
    # applies cable loom short SIG1 -> Vcc
    g_hilCtrl.write('DOUT.DOUT0: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def removeCableShort1(delay):
    # removes cable loom short SIG1 -> Vcc
    g_hilCtrl.write('DOUT.DOUT0: LOW')
    if delay > 0:
        time.sleep(delay)
    
def applyCableShort2(delay):
    # applies cable loom short SIG2 -> Vcc
    g_hilCtrl.write('DOUT.DOUT3: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def removeCableShort2(delay):
    # removes cable loom short SIG2 -> Vcc
    g_hilCtrl.write('DOUT.DOUT3: LOW')
    if delay > 0:
        time.sleep(delay)
    
def getBrakeIndicatorStatus():
    returnValue = 'OFF'
    if g_hilCtrl.read('DIN.DIN2') == 'HIGH':
        returnValue = 'ON'
    return returnValue

def getBrakeWarningStatus():
    returnValue = 'OFF'
    if g_hilCtrl.read('DIN.DIN1') == 'HIGH':
        returnValue = 'ON'
    return returnValue

def verifyBrakeLight(expected):
    actual = getBrakeIndicatorStatus()
    if actual != expected:
        print('Error! ', actual, ' ', expected)
        return False
    return True

def verifyBrakeLightOn():
    actual = getBrakeIndicatorStatus()
    if actual == "ON":
        return True
    return False

def verifyBrakeLightOff():
    actual = getBrakeIndicatorStatus()
    print(actual)
    if actual == "OFF":
        return True
    return False
    
def verifyWarningLight(expected):
    actual = getBrakeWarningStatus()
    if actual != expected:
        print('Error! ', actual, ' ', expected)
        return False
    return True

def verifyWarningLightOn():
    actual = getBrakeWarningStatus()
    print(actual)
    if actual == "ON":
        return True
    return False
    
def verifyWarningLightOff():
    actual = getBrakeWarningStatus()
    print(actual)
    if actual == "OFF":
        return True
    return False
    
def getBrakeSignalLightState():
    return brakeIndicatorLedState.getLedStatus()
    
def getBrakeWarningLightState():
    return brakeErrorLedState.getLedStatus()

def postCondSetBrakeSignalLed(state):
    if state == "ON":
        brakeIndicatorLedState.ledOn()
    if state == "OFF":
        brakeIndicatorLedState.ledOff()

def postCondSetBrakeWarningLightState(state):
    if state == "ON":
        brakeErrorLedState.ledOn()
    if state == "OFF":
        brakeErrorLedState.ledOff()

def createReport(results):
    print('Report:')
    for testName in results:
        print(testName, ': ')
        singleTestResults = results[testName]
        for testStatus in singleTestResults:
            print('    ' + str(testStatus))
