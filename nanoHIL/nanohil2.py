
# This module should be implemented by the user, who knows the meaning of
# target I/O signals and variables. Basically there are two possibilities
# with iSystem tools:
# - read/write I/O signals with iSystem IO module attached on BlueBox
# - read/write variables on target and stub I/O functions on target and
#   define their return values.

# (c) iSYSTEM AG, Mar. 2016

# Configured for EPB-Demo project

from __future__ import print_function

import time
import os
import sys
import isystem.connect as ic
import teststatus as ts
import junitreport as jr

g_debug = None
g_dataCtrl = None
g_hilCtrl = None
#_exit_testing = False
#g_testStatus = None

# This stores the state of the brake signal light for precondition situations 
# during testing
#g_brakeSignalLightState = ''
# This stores the state of the brake warning light for precondition situations 
# during testing
#g_brakeWarningLightState = ''
# This stores the state of the entire test process
#g_testStatus = ''



class TestResult:
    """
    This class contains result of one test case.
    """
    
    def __init__(self, testId, testedItem,
                 isError = False,
                 isFailure = False,
                 errorFailureType = '',
                 errorFailureDesc = ''):
        
        self._testId = testId
        self._testedItem = testedItem
        self._isError = isError
        self._isFailure = isFailure
        self._errorFailureType = errorFailureType
        self._errorFailureDesc = errorFailureDesc

    def getTestId(self):
        return self._testId

    def getTestedItem(self):
        return self._testedItem
        
    def isError(self):
        return self._isError
        
    def isFailure(self):
        return self._isFailure
        
    def getErrorFailureType(self):
        return self._errorFailureType
    
    def getErrorFailureDesc(self):
        return self._errorFailureDesc


class _TestReportStatistics:
    """
    This class contains test statistics. For internal usage only.
    """
    def __init__(self, noOfErrors, noOfFailures):
        self._noOfErrors = noOfErrors
        self._noOfFailures = noOfFailures

    def getNoOfErrors(self):
        return self._noOfErrors
        
    def getNoOfFailures(self):
        return self._noOfFailures
        

def saveTestResultsAsJUnit(fileName, testSuiteName, testResults):
    """
    This method saves test results in Junit format, so that it can be parsed
    by Jenkins/Hudson. This method creates file with one test suite.
    
    For details about the format see:
    https://stackoverflow.com/questions/4922867/junit-xml-format-specification-that-hudson-supports

    XSD:
    https://svn.jenkins-ci.org/trunk/hudson/dtkit/dtkit-format/dtkit-junit-model/src/main/resources/com/thalesgroup/dtkit/junit/model/xsd/junit-4.xsd

    Parameters:

    fileName - name of file to save report to
    testSuiteName - name of test suite which is used for attribute name:
                    '<testsuite name="' + testSuiteName + '" ...
    testResults - list of TestResult instances
    
    Example of generated file:
    <testsuites name="testIDEATestSuite" tests="3" errors="0" failures="0">
      <testsuite name="testIDEATestSuite" tests="3" errors="0" failures="0">
        <testcase classname="add_int" name="test-0"/>
        <testcase classname="add_int" name="test-1"
          <error type="exception"/>Invalid value!</error>
        </testcase>
        <testcase classname="max_int" name="test-2">
          <failure type="expression"></failure>
        </testcase>
      </testsuite>
    </testsuites>
    """

    reportStats = _getReportStatistics(testResults)

    with open(fileName, 'w') as of:
        _saveTestResultsAsJUnitStart(of,
                                     testSuiteName,
                                     len(testResults),
                                     reportStats.getNoOfErrors(),
                                     reportStats.getNoOfFailures())

        _saveTestResultsAsJUnitForTestSuite(of,
                                            testSuiteName,
                                            testResults,
                                            reportStats)
        _saveTestResultsAsJUnitEnd(of)


def _saveTestResultsAsJUnitStart(of, testSuitesName, noOfTests, noOfErrors,
                                 noOfFailures):
    """
    This method saves test results in Junit format, so that it can be parsed
    by Jenkins/Hudson. Typical usage:

        saveTestResultsAsJUnitStart(...)
        saveTestResultsAsJUnitForTestSuite(...)
        saveTestResultsAsJUnitEnd()
    

    Parameters:
    of - opened file stream
    testSuitesName - values for the 'name' attribute of tags 'testsuites' and
                     'testsuite'
    noOfTests - number of all tests in test suite
    noOfErrors - number of errors
    noOfFailures - number of failures

    For details about format see method _saveTestResultsAsJUnit().
    """

    of.write('<?xml version="1.0" encoding="UTF-8"?>\n')

    of.write('<testsuites name="' + testSuitesName + '" tests="' +
             str(noOfTests) + '" errors="' +
             str(noOfErrors) + '" failures="' +
             str(noOfFailures) + '">\n')

    
def _saveTestResultsAsJUnitForTestSuite(of, testSuiteName, testResults, reportStats):
    """
    This method saves test results in JUnit format, so that it can be parsed
    by Jenkins/Hudson for one test suite. Typical usage:

        saveTestResultsAsJUnitStart(...)
        saveTestResultsAsJUnitForTestSuite(...)
        saveTestResultsAsJUnitEnd()
    
    See method _saveTestResultsAsJUnitStart() for details.
    """

    of.write('  <testsuite name="' + testSuiteName + '" tests="' +
                str(len(testResults)) + '" errors="' +
                str(reportStats.getNoOfErrors()) + '" failures="' +
                str(reportStats.getNoOfFailures()) + '">\n')

    for tr in testResults:

        of.write('    <testcase classname="' + tr.getTestedItem() + '" name="' +
                 tr.getTestId() + '">\n')

        if tr.isError():
            of.write('      <error type="' + tr.getErrorFailureType() + '">')
            of.write(_replaceXMLEntities(tr.getErrorFailureDesc()))
            of.write('</error>\n')
            
        if tr.isFailure():
            of.write('      <failure type="' + tr.getErrorFailureType() + '">')
            of.write(_replaceXMLEntities(tr.getErrorFailureDesc()))
            of.write('</failure>\n')

        of.write('    </testcase>\n')
    of.write('  </testsuite>\n')
                 

def _saveTestResultsAsJUnitEnd(of):
    """
    This method adds the last tag to XML file in Jenkins/Hudson format.
    See method saveTestResultsAsJUnitStart() for details.
    """
    of.write('</testsuites>\n')


def _getReportStatistics(testResults):

    noOfErrors = 0
    noOfFailures = 0
    
    for tr in testResults:
        
        if tr.isError():
            noOfErrors += 1
            
        if tr.isFailure():
            noOfFailures += 1

    return _TestReportStatistics(noOfErrors, noOfFailures)

    
def _replaceXMLEntities(message):
    message = message.replace('<', '&lt;')
    message = message.replace('>', '&gt;')
    message = message.replace('&', '&amp;')
    message = message.replace('"', '&quot;')
    message = message.replace("'", '&apos;')
    return message


class overallTestStatus:
    """
    Keeps track of overall status of all tests
    """
    
    def __init__(self):
        self.testStatus = '## nano-HIL TESTS - PASSED ##'
        self.failureCounter = 0
        
    def outputTestStatus(self):
        print(self.testStatus)
        
    def testFailed(self):
        self.testStatus = '!!nano-HIL TESTS - FAILED!!'
        self.failureCounter += 1
        
    def outputFailureCounter(self):
        print(self.failureCounter)

class ledState:
    """
    Keeps track of status of LEDs
    """

    def __init__(self):
        self.ledStatus = 'OFF'
        
    def ledOn(self):
        self.ledStatus = 'ON'
        
    def ledOff(self):
        self.ledStatus = 'OFF'
        
    def resetLedStatus(self):
        self.ledStatus = 'OFF'
        
    def getLedStatus(self):
        return self.ledStatus


        
        
def initSystem():
    """
    Initializes isystem.connect and nano-HIL system
    """
    global g_debug, g_dataCtrl, g_hilCtrl
    #global g_brakeSignalLightState, g_brakeWarningLightState
    global testStatus, brakeIndicatorLedState, brakeErrorLedState
    
    global _exit_testing
    
    print('isystem.connect version: ' + ic.getModuleVersion())

    """
    Find name of workspace in current dir - if more than one, take first found
    If none are found, exit script
    """
    
    workspaceFound = False;
    
    for f in os.listdir("./"):
        if f.endswith(".xjrf"):
            print("Found following winIDEA workspace -", f)
            workspaceFound = True;
            break

    if not workspaceFound:
        print("!ERROR! - could not find winIDEA workspace.")
        sys.exit()

    cmgr = ic.ConnectionMgr()
    cmgr.connectMRU(f)

    g_debug = ic.CDebugFacade(cmgr)

    cpuStatus = g_debug.getCPUStatus()

    print('Downloading binary...')
    g_debug.download()
    
    print('Resetting target...')
    g_debug.reset()
    
    g_dataCtrl = ic.CDataController(cmgr)
    g_hilCtrl = ic.CHILController(cmgr)
    
    #g_testStatus = '## nano-HIL TESTS - PASSED ##'
    #g_brakeSignalLightState = 'OFF'
    #g_brakeWarningLightState = 'OFF'
    
    print('Resetting overall test status...')
    testStatus = overallTestStatus()
    
    print('Resetting brake indicator LED state...')
    brakeIndicatorLedState = ledState()
    #print(brakeIndicatorLedState.getLedStatus())
    
    print('Resetting brake error LED state...')
    brakeErrorLedState = ledState()
    #print(brakeErrorLedState.getLedStatus())
    
    _exit_testing = False


def resetAndRun():
    print('Reset and run.')
    g_debug.reset()
    g_debug.run()
    #print(g_testStatus)
    
    print('Clearing LED states.')
    brakeIndicatorLedState.resetLedStatus()
    #print(brakeIndicatorLedState.getLedStatus())
    brakeErrorLedState.resetLedStatus()
    #print(brakeErrorLedState.getLedStatus())

    
def deinitSystem():
    """
    Disconnect from nano-HIL system
    """
    print('Test case finished')
    
    g_debug.stop()
    
    initNanoHil()
    #print(g_testStatus)

def initNanoHil():
    # reset inputs in target
    g_hilCtrl.write('DOUT.DOUT0: LOW')
    g_hilCtrl.write('DOUT.DOUT1: LOW')
    g_hilCtrl.write('DOUT.DOUT2: LOW')
    g_hilCtrl.write('DOUT.DOUT3: LOW')
    g_hilCtrl.write('DOUT.DOUT4: LOW')
    g_hilCtrl.write('DOUT.DOUT5: LOW')
    g_hilCtrl.write('DOUT.DOUT6: LOW')
    g_hilCtrl.write('DOUT.DOUT7: LOW')
    
    time.sleep(1)
    
def initStartDelay():
    # user can define initial start-up delay here
    return 2

def testHasFailed():
    testStatus.testFailed()
    print('Failure detected - ', end="")
    testStatus.outputFailureCounter()
    
def getOverallTestStatus():
    testStatus.outputTestStatus()

# END OF PREDEFINED SECTION    

def turnIgnitionOn(delay):
    # turns ignition on
    g_hilCtrl.write('DOUT.DOUT5: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def turnIgnitionOff(delay):
    # turns ignition off
    g_hilCtrl.write('DOUT.DOUT5: LOW')
    if delay > 0:
        time.sleep(delay)

def vehicleInMotion(delay):
    # applies "In Motion" switch
    g_hilCtrl.write('DOUT.DOUT6: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def vehicleHalted(delay):
    # releases "In Motion" switch
    g_hilCtrl.write('DOUT.DOUT6: LOW')
    if delay > 0:
        time.sleep(delay)

def pressApplyBrake(delay):
    # presses "Apply Brake" switch
    g_hilCtrl.write('DOUT.DOUT4: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def releaseApplyBrake(delay):
    # releases "Apply Brake" switch
    g_hilCtrl.write('DOUT.DOUT4: LOW')
    if delay > 0:
        time.sleep(delay)

def pressReleaseBrake(delay):
    # presses "Release Brake" switch
    g_hilCtrl.write('DOUT.DOUT7: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def releaseReleaseBrake(delay):
    # releases "Release Brake" switch
    g_hilCtrl.write('DOUT.DOUT7: LOW')
    if delay > 0:
        time.sleep(delay)

def applyCableBreak1(delay):
    # applies cable loom break to Vcc
    g_hilCtrl.write('DOUT.DOUT1: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def removeCableBreak1(delay):
    # removes cable loom break to Vcc
    g_hilCtrl.write('DOUT.DOUT1: LOW')
    if delay > 0:
        time.sleep(delay)
    
def applyCableBreak2(delay):
    # applies cable loom break to SIG2
    g_hilCtrl.write('DOUT.DOUT2: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def removeCableBreak2(delay):
    # removes cable loom break to SIG2
    g_hilCtrl.write('DOUT.DOUT2: LOW')
    if delay > 0:
        time.sleep(delay)

def applyCableShort1(delay):
    # applies cable loom short SIG1 -> Vcc
    g_hilCtrl.write('DOUT.DOUT0: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def removeCableShort1(delay):
    # removes cable loom short SIG1 -> Vcc
    g_hilCtrl.write('DOUT.DOUT0: LOW')
    if delay > 0:
        time.sleep(delay)
    
def applyCableShort2(delay):
    # applies cable loom short SIG2 -> Vcc
    g_hilCtrl.write('DOUT.DOUT3: HIGH')
    if delay > 0:
        time.sleep(delay)
    
def removeCableShort2(delay):
    # removes cable loom short SIG2 -> Vcc
    g_hilCtrl.write('DOUT.DOUT3: LOW')
    if delay > 0:
        time.sleep(delay)
    
def getBrakeIndicatorStatus():
    returnValue = 'OFF'
    if g_hilCtrl.read('DIN.DIN2') == 'HIGH':
        returnValue = 'ON'
    return returnValue

def getBrakeWarningStatus():
    returnValue = 'OFF'
    if g_hilCtrl.read('DIN.DIN1') == 'HIGH':
        returnValue = 'ON'
    return returnValue

def verifyBrakeLight(expected):
    actual = getBrakeIndicatorStatus()
    if actual != expected:
        print('Error! ', actual, ' ', expected)
        return False
    return True

def verifyBrakeLightOn():
    actual = getBrakeIndicatorStatus()
    if actual == "ON":
        return True
    return False

def verifyBrakeLightOff():
    actual = getBrakeIndicatorStatus()
    print(actual)
    if actual == "OFF":
        return True
    return False
    
def verifyWarningLight(expected):
    actual = getBrakeWarningStatus()
    if actual != expected:
        print('Error! ', actual, ' ', expected)
        return False
    return True

def verifyWarningLightOn():
    actual = getBrakeWarningStatus()
    print(actual)
    if actual == "ON":
        return True
    return False
    
def verifyWarningLightOff():
    actual = getBrakeWarningStatus()
    print(actual)
    if actual == "OFF":
        return True
    return False
    
def getBrakeSignalLightState():
    return brakeIndicatorLedState.getLedStatus()
    
def getBrakeWarningLightState():
    return brakeErrorLedState.getLedStatus()

def postCondSetBrakeSignalLed(state):
    if state == "ON":
        brakeIndicatorLedState.ledOn()
    if state == "OFF":
        brakeIndicatorLedState.ledOff()

def postCondSetBrakeWarningLightState(state):
    if state == "ON":
        brakeErrorLedState.ledOn()
    if state == "OFF":
        brakeErrorLedState.ledOff()

def createReport(results, junit):
    junitResults = []
    junitstring = ""
    
    count = 0
    
    if junit == True:
        print('Creating junit report')
    
    print('Report:')
    
    for testName in results:
        count += 1
        print(testName, ': ')
                
        singleTestResults = results[testName]
        
        junitstring = ""
        
        for testStatus in singleTestResults:
            print('    ' + str(testStatus))
            junitstring = junitstring + str(testStatus) + "; "

        if junit:
            strCount = str(count)
            
            #passFail = str(testStatus)
            
            if junitstring.find("FAILED") < 0:
                junitResults.append(jr.TestResult(strCount.rjust(2, "0"), testName))
            else:
                junitResults.append(jr.TestResult(strCount.rjust(2, "0"), testName,
                        isFailure = True,
                        errorFailureType = 'HIL test',
                        errorFailureDesc = junitstring))

    if junit:
        jr.saveTestResultsAsJUnit('testReport.junit', 'Edge HIL test', junitResults)