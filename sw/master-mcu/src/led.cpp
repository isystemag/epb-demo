#include "Arduino.h"
#include "constants.h"

static boolean statusLed = false;
static int statusLedPin = 0;
static boolean errorLed = false;
static int errorLedPin = 0;

static boolean ledsTurnedOn = false;

static void updateStatusLed(void) {
    if (ledsTurnedOn == true) {
        if (statusLed == true) {
            digitalWrite(statusLedPin, HIGH);
        } else {
            digitalWrite(statusLedPin, LOW);
        }
    } else {
        digitalWrite(statusLedPin, LOW);
    }
}

static void updateErrorLed(void) {
    if (ledsTurnedOn == true) {
        if (errorLed == true) {
            digitalWrite(errorLedPin, HIGH);
        } else {
            digitalWrite(errorLedPin, LOW);
        }
    } else {
        digitalWrite(errorLedPin, LOW);
    }
}

void ledsOn(void) {
    ledsTurnedOn = true;
    updateStatusLed();
    updateErrorLed();
}

void ledsOff(void) {
    ledsTurnedOn = false;
    updateStatusLed();
    updateErrorLed();
}

void ledsForceOn(void) {
    digitalWrite(statusLedPin, HIGH);
    digitalWrite(errorLedPin, HIGH);
}

void ledsForceOff(void) {
    digitalWrite(statusLedPin, LOW);
    digitalWrite(errorLedPin, LOW);
}

void initBrakeStatusLed(int led) {
    statusLedPin = led;
    pinMode(statusLedPin, OUTPUT);
    digitalWrite(statusLedPin, LOW);
    statusLed = false;
}

void initBrakeErrorLed(int led) {
    errorLedPin = led;
    pinMode(errorLedPin, OUTPUT);
    digitalWrite(errorLedPin, LOW);
    errorLed = false;
}

void brakeStatusLedOn(void) {
    statusLed = true;
    updateStatusLed();
}

void brakeStatusLedOff(void) {
    statusLed = false;
    updateStatusLed();
}

void brakeErrorLedOn(void) {
    errorLed = true;
    updateErrorLed();
}

void brakeErrorLedOff(void) {
    errorLed = false;
    updateErrorLed();
}
