/*******************************************************************************
* Project: EPB - Electric Parking Brake
*
* iSYSTEM AG, Markus Huber and Stuart Cording
*
*******************************************************************************/

/******************************************************************************/
/** \file brake.h
* 
* This module defines the functions to simulate the state of a brake for the EPB 
* project.
*
*******************************************************************************/

/*******************************************************************************
*
*                               EPB - BRAKE MODULE
*
*******************************************************************************/
#ifndef Brake_h
#define Brake_h

/******************************************************************************/
/** \defgroup BrakeModuleFunction Brake Initialise and Control Functions
* Brake Module's functions used to initialise and control the state of an
* 'imaginary' brake.
* @{ 
*******************************************************************************/

/*******************************************************************************
*                                 INCLUDE FILES
*******************************************************************************/
#include "Arduino.h"


/*******************************************************************************
*                                    EXTERNS
*******************************************************************************/


/*******************************************************************************
*                             DEFAULT CONFIGURATION
*******************************************************************************/


/*******************************************************************************
*                               DEFINES AND MACROS
*******************************************************************************/

/******************************************************************************/
/** \def BRAKE_APPLIED
* The BRAKE_APPLIED definition is used to determine that the brake is in the 
* applied state. 10 is chosen as it is not a value that is likely to appear in
* SRAM by itself.
*******************************************************************************/
#define BRAKE_APPLIED 10

/******************************************************************************/
/** \def BRAKE_RELEASED
* The BRAKE_RELEASED definition is used to determine that the brake is in the 
* released state. 20 is chosen as it is not a value that is likely to appear in
* SRAM by itself.
*******************************************************************************/
#define BRAKE_RELEASED 20


/*******************************************************************************
*                                   DATA TYPES
*******************************************************************************/


/*******************************************************************************
*                                GLOBAL VARIABLES
*******************************************************************************/


/*******************************************************************************
*                              FUNCTION PROTOTYPES
*******************************************************************************/

/******************************************************************************/
/** \cond
* Function - brakeInit()
* \endcond
*
* \brief Initialises the brake module.
*
* Initialises the internal status of this Brake Module. Must be called prior to
* using any other functions in the Brake Module.
*
* \sa applyBrake() releaseBrake()
*
* \retval   true - Brake Module initialised successfully
* \retval   false - Brake Module failed to initialise
*
* \note
* -# Must be called before using any functions in the Brake Module.
*
*******************************************************************************/
boolean brakeInit(void);

/******************************************************************************/
/** \cond
* Function - applyBrake()
* \endcond
*
* \brief Places the brake in the "applied" state.
*
* Sets the state of the brake into the "applied" state.
*
* \sa brakeInit() releaseBrake()
*
* \retval   true - Brake Module initialised successfully
* \retval   false - Brake Module failed to initialise
*
* \note
* -# brakeInit() must be called before using this function.
*
*******************************************************************************/  
void applyBrake(void);

/******************************************************************************/
/** \cond
* Function - releaseBrake()
* \endcond
*
* \brief Places the brake in the "released" state.
*
* Sets the state of the brake into the "release" state.
*
* \sa brakeInit() applyBrake()
*
* \note
* -# brakeInit() must be called before using this function.
*
*******************************************************************************/
void releaseBrake(void);

/******************************************************************************/
/** \cond
* Function - getBrakeStatus()
* \endcond
*
* \brief Returns current state of brake.
*
* Initialises all global variables needed by the CBUFF module. Must be called 
* before using any of the other functions in this module. No check will be made
* to ensure that you did actually initialise the module before using it, so it
* is up to you!
*
* \sa storeBrakeStatus()
*
* \retval   #BRAKE_APPLIED - brake is currently in the "applied" state
* \retval   #BRAKE_RELEASED - brake is currently in the "released" state
*
* \note
* -# brakeInit() must be called before using this function.
*
*******************************************************************************/
byte getBrakeStatus();

/******************************************************************************/
/** \cond
* Function - storeBrakeStatus()
* \endcond
*
* \brief !!NOT IMPLEMENTED!! Stores current state of brake to TBD location.
*
* 
*
* \sa brakeInit() getBrakeStatus()
*
* \retval   true - TBD
* \retval   false - TBD
*
* \note
* -# brakeInit() must be called before using this function.
*
*******************************************************************************/
boolean storeBrakeStatus();


/*******************************************************************************
*                              CONFIGURATION ERRORS
*******************************************************************************/


/*******************************************************************************
*
*                             EPB - BRAKE MODULE END
*
*******************************************************************************/

/**
* @} 
*******************************************************************************/

#endif

