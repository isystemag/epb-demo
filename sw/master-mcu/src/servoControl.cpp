#include "servoControl.h"

static int servoPos = OPEN_LIMIT;

int updateServoPos(byte brakeState){
  
    // change position, according to brakestate
    if(brakeState == BRAKE_RELEASED){
        servoPos += STEP_VALUE;
    } else if(brakeState == BRAKE_APPLIED){
        servoPos -= STEP_VALUE;
  } else {
    Serial.print("Error: BrakeState-Evaluation!");
  }
  
  if (servoPos <= CLOSED_LIMIT) {
      servoPos = CLOSED_LIMIT;
  }
  
  if (servoPos >= OPEN_LIMIT) {
      servoPos = OPEN_LIMIT;
  }
    
  //Serial.println(servoPos);
  
  return servoPos;
}
