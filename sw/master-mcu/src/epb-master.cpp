/*******************************************************************************
* Project: EPB - Electric Parking Brake
*
* iSYSTEM AG, Markus Huber and Stuart Cording
*
*******************************************************************************/
/******************************************************************************/
/**
* \file epb-master.cpp
* \mainpage
*
* \section intro Introduction
* The aim of the EPB project was to develop the hardware and software to 
* safely evaluate the input switch of the EPB so as it could fulfil the ASIL
* level as determined by ISO 26262. For visual effect, a brake is simulated 
* using a standard off-the-shelf model servo.
*
* The project is broken down into two parts; a master element that would be the
* main MCU in the EPB; and a slave element whose responsibility it is to simply
* monitor the same signals and provide its evaluation of the signals to the
* master. In the event that the two values fail to be interpreted the same, 
* this is condisered to be a fault.
*
* This project implements the master MCU in an EPB electronic control unit. The
* master is responsible for evaluating the switch inputs and determining if
* the brake should be applied or released. Additionally, the master checks for
* faults in the wiring and puts the EPB in a 'safe state' until such time as the
* EPB can be repaired.
*
* \section contactInfo Contact information
* For more information and the latest release, please visit this projects home
* page at http://bitbucket.org/isystemag/epb-demo
* To participate in the project or for other enquiries, please contact Stuart
* Cording at stuart.cording@isystem.com or +49 8138 6971 59 or visit us at 
* http://www.isystem.com .
*
* \author Markus Huber
* \author Stuart Cording AKA CODINGHEAD
*
********************************************************************************
* \note
* - Currently no notes (Aug 2015)
*******************************************************************************/

#include "Arduino.h"
#include "Servo.h"
#include <Wire.h>
#include "brake.h"
#include "servoControl.h"
#include "constants.h"
#include "evaluation.h"
#include "readSensors.h"
#include "led.h"

/*******************************************************************************
* Variable declarations initialization / resets
*******************************************************************************/

static Servo brakeSimulator;
static byte brakeStatus;
static byte aliveCounter = 20; //TODO: start with higher value, e.g. 20
static boolean ignitionOn = false;
static boolean noError = true;
static int ignitionSense = 1; //ignition off
static boolean firstRequest = true;
enum EpbState {
    EPB_DELIVERY_STATE,
    EPB_INIT,
    IGNITION_OFF,
    IGNITION_OFF_WAITING,
    IGNITION_ON_INIT,
    IGNITION_ON,
    EPB_ERROR,
    EPB_ERROR_IGN_ON_1,
    EPB_ERROR_IGN_ON_2,
    EPB_ERROR_IGN_OFF
};
static EpbState epbState = EPB_DELIVERY_STATE;

/*********************************************************
**********************************************************
**                 Method declarations                  **
**********************************************************
*********************************************************/

/*********************************************************
* systemTest()
*
* Test mode to display raw values from ADC inputs and
* Motion Switch status.
*
* Mode is entered when Ignition is ON and Motion is
* PRESSED
*********************************************************/
void systemTest() {
    boolean runTest = true;
    boolean csvMode = false;
    
    float vccSense = 0;          // vccSense - measured VCC-control voltage 
    float gndSense = 0;          // gndSense - measured GND-control voltage
    float switch1 = 0;           // switch1 - measured voltage of first switch-part
    float switch2 = 0;           // switch2 - measured voltage of second switch-part
    static unsigned int vccSenseInt = 0;          // vccSense - measured VCC-control voltage 
    static unsigned int gndSenseInt = 0;          // gndSense - measured GND-control voltage
    static unsigned int switch1Int = 0;           // switch1 - measured voltage of first switch-part
    static unsigned int switch2Int = 0;           // switch2 - measured voltage of second switch-part
    int throttleSense = 1;              // 1 = no throttle
    int count = 0;
    
    Serial.println("System Test Entered");
    Serial.println();
    Serial.println("Turn ignition off to return to application");
    Serial.println();
    
    do {
        throttleSense = readThrottleSense();
        
        // Use Motion Switch to switch between CSV output and
        // raw data with Voltages
        if (throttleSense == 0) {
            if (csvMode == false) {
                csvMode = true;
                Serial.println("VCC,GND,SW1,SW2,MOVING");
            } else {
                csvMode = false;
            }
        }
        
        if (csvMode == false) {
            vccSense = readVccSense();
            vccSenseInt = readVccSenseInt();
            
            gndSense = readGndSense();
            gndSenseInt = readGndSenseInt();
            
            switch1 = readSwitchSense1();
            switch1Int = readSwitchSense1Int();
            
            switch2 = readSwitchSense2();
            switch2Int = readSwitchSense2Int();
            
            Serial.print(vccSenseInt);
            Serial.print(" ");
            Serial.print(gndSenseInt);
            Serial.print(" ");
            Serial.print(switch1Int);
            Serial.print(" ");
            Serial.print(switch2Int);
            Serial.print("   ");
            Serial.print(vccSense);
            Serial.print(" ");
            Serial.print(gndSense);
            Serial.print(" ");
            Serial.print(switch1);
            Serial.print(" ");
            Serial.print(switch2);
            Serial.print("   ");
            Serial.print(throttleSense);
            Serial.println();
        } else {
            vccSenseInt = readVccSenseInt();
            gndSenseInt = readGndSenseInt();
            switch1Int = readSwitchSense1Int();
            switch2Int = readSwitchSense2Int();
            
            Serial.print(vccSenseInt);
            Serial.print(",");
            Serial.print(gndSenseInt);
            Serial.print(",");
            Serial.print(switch1Int);
            Serial.print(",");
            Serial.print(switch2Int);
            Serial.print(",");
            Serial.print(throttleSense);
            Serial.println();
        }
            
        // Blink LEDs to show they are working
        ++count;
        
        if (count == 1) {
            digitalWrite(INFO_LED, HIGH);
            digitalWrite(ERROR_LED, LOW);
            digitalWrite(LED, LOW);
        } else if (count == 2) {
            digitalWrite(INFO_LED, LOW);
            digitalWrite(ERROR_LED, HIGH);
            digitalWrite(LED, LOW);
        } else if (count == 3) {
            digitalWrite(INFO_LED, LOW);
            digitalWrite(ERROR_LED, LOW);
            digitalWrite(LED, HIGH);
        } else if (count == 4) {
            digitalWrite(INFO_LED, LOW);
            digitalWrite(ERROR_LED, LOW);
            digitalWrite(LED, LOW);
        } else if (count == 5) {
            count = 0;
        }
        
        // If Ignition is switched OFF, it signals end
        // of testing
        ignitionSense = readIgnitionSense();
        ignitionOn = evaluateIgnitionSense(ignitionSense);
        
        if (ignitionOn == false) {
            runTest = false;
        } else {
            delay(250);
        }
        
    } while (runTest == true);
    
}

void repairBrakes(){
    brakeErrorLedOff();
    Serial.println("### BRAKES REPAIRED! ###");
}

boolean waitForIgnitionOn(){
    boolean initialiseStatus = false;

    boolean throttlePressed = false;
    int throttleSense = 1;

    //Serial.println("Ignition OFF!");
    //Serial.println("Waiting for ignition to be turned on");

    //Turn LEDs off, when ignition is turned off
    ledsOff();

    ignitionSense = readIgnitionSense();
    ignitionOn = evaluateIgnitionSense(ignitionSense);

    // If ignition is turned on while "in motion", enter test mode
    if (ignitionOn == true) {
        throttleSense = readThrottleSense();
        throttlePressed = evaluateThrottleSense(throttleSense);

        if (throttlePressed == true) {
            systemTest();
        } else {
            // Otherwise, note that ignition is on
            initialiseStatus = true;
            Serial.println("Ignition ON!");
            
            brakeErrorLedOn();
            ledsOn();
            delay(1000);
            brakeErrorLedOff();
            
            // Checks if brake-initialization was successful
            if (brakeInit() != true) {
                Serial.println("Can't initialise brake - stopping.");
                while(1);
            } else {
                // Initialization successful!
            }
        }
    }

    return initialiseStatus;
}

// Supporting function for printing simple error-messages.
void reportError(String errorType){
  Serial.println(errorType);
}

// Supporting function for printing messages, if switch is not in neutral position.
void reportSwitchPress(String pressedType){
  Serial.println(pressedType);
}

boolean checkSlaveResults(systemState masterSysState){

    boolean checkSuccessful = false;
    boolean connectionAndSlaveOk = false;
    boolean resultsConsistent = false;
    systemState slaveSysState = UNDEFINED_STATE;
    //Wire.requestFrom(8, 2);      // request 2 bytes from slave device #8
    int j = 0;
    byte newAliveCounter;

#if 0
  
    while (Wire.available()) { // slave may send less than requested
        byte answerPart = Wire.read();
        if(j == 1){
            slaveSysState = (systemState)answerPart;
        }
        if(j == 0){
            newAliveCounter = answerPart;
        }
    
        j++;
    
    }
    
    if (firstRequest){
        connectionAndSlaveOk = true;
        aliveCounter = newAliveCounter;
        
        if (aliveCounter >= 255){
            aliveCounter = 20; //reset
        }
        firstRequest = false;
    }

    if (newAliveCounter == (aliveCounter + 1)){
        connectionAndSlaveOk = true;
        aliveCounter = newAliveCounter;
        
        if(aliveCounter >= 255){
            aliveCounter = 20; //reset
        }
    }
    
    if(masterSysState == slaveSysState){
        resultsConsistent = true;
    }


    checkSuccessful = true;//(connectionAndSlaveOk && resultsConsistent);

    Serial.println("Master:     Slave:   ");
    Serial.print(masterSysState);
    Serial.print("               ");
    Serial.print(slaveSysState);
    Serial.println();
    Serial.print(aliveCounter);
    Serial.print("               ");
    Serial.print(newAliveCounter);
    Serial.println();

    Serial.println("Successful?: ");
    Serial.println(checkSuccessful);
#endif

    return (true);
}


/********************************************************* 
**********************************************************
**                      setup()                         **
**********************************************************
*********************************************************/

void setup() {
    brakeSimulator.attach(SERVO_PIN);           //Attach Servo to PIN 9~
    pinMode(LED, OUTPUT);                       // Initializing led-pin as an output.

    ledsOff();
    initBrakeStatusLed(INFO_LED);
    initBrakeErrorLed(ERROR_LED);

    pinMode(IGNITION_PIN, INPUT);
    pinMode(THROTTLE_PIN, INPUT);

    analogReadResolution(ADC_READ_RESOLUTION);  // Sets the size (in bits) of the value returned 
                                              // by analogRead(). 
    Serial.begin(115200);                       // Sets the data rate for communication with the 
                                              // computer (115200)

    // Set up brake to delivery state
    brakeSimulator.write(OPEN_LIMIT);
  
  //waitForIgnitionOn();
    
    // Output message
    Serial.println("iSYSTEM AG EPB Demonstrator");
    Serial.println("===========================");
    Serial.println();
    Serial.println("Developed by Markus Huber");
    Serial.println("and Stuart Cording");
    Serial.println();
    
  
    //Wire.begin();        // join i2c bus (address optional for master)
}

/********************************************************* 
**********************************************************
**                       loop()                         **
**********************************************************
*********************************************************/

void loop() {

    static int count = 0;
    static int oldCount = 0;
    //TODO: check which value is appropriate as default for startTime
    static unsigned long startTime;
    static unsigned long passedTime = 0;
    
    static boolean emergencyBrakeApplied = false;
    static boolean throttlePressed = false;
    static int servoPos = OPEN_LIMIT;                       // servoPos - position of servo - [20,160]
    
    int throttleSense = 1;                            // 1 = no throttle
  
    if (epbState == EPB_DELIVERY_STATE) {
        Serial.println("EPB is in delivery state - initialising...");
        epbState = EPB_INIT;
    } else if (epbState == EPB_INIT) {
        if (waitForIgnitionOn() == true) {
            epbState = IGNITION_ON_INIT;
            Serial.println("System first initialisation completed");
            Serial.println();
        }
    } else if (epbState == IGNITION_ON_INIT) {
        Serial.println("Ignition on");
        // Run a test to make sure LEDs work
        ledsForceOn();
        delay(650);
        ledsForceOff();
        epbState = IGNITION_ON;
        
    } else if (epbState == IGNITION_ON) {
    
        /********************************************************* 
        **    Variable declarations initialization / resets     **
        *********************************************************/

        static int vccSense = 0;                               // vccSense - measured VCC-control voltage 
        static int gndSense = 0;                               // gndSense - measured GND-control voltage
        static int switch1 = 0;                                // switch1 - measured voltage of first switch-part
        static int switch2 = 0;                                // switch2 - measured voltage of second switch-part
        
        //int throttleSense = 1;                            // 1 = no throttle
        boolean vccStatusOk = false;                          // vccStatusOk - true, if there is no short or opening in the signal circuit
        boolean gndStatusOk = false;                          // gndStatusOk - true, if there is no short or opening in the signal circuit
        voltInterval switchStatus1 = UNDEFINED_INTERVAL;  // switchStatus1 - Indicates, in which range the first measured switch-value was.
        voltInterval switchStatus2 = UNDEFINED_INTERVAL;  // switchStatus2 - Indicates in which range the second measured switch-value was.
        //boolean throttlePressed = false;
        systemState sysState = UNDEFINED_STATE;           // sysState - code for 'switch-position' or hardware-failure-type          
 
        // Turn on LEDs to previous state
        ledsOn();
        
        if(oldCount < count){
          oldCount = count;
        } else {
          count = 0;
          oldCount = 0;
          passedTime = 0;
        }
    
        Serial.print(count);
        Serial.print(" : ");
        Serial.println(passedTime);
           
        /********************************************************* 
        **              Methods / "Working Part"                **
        *********************************************************/
        
        // Switch led on and off, to signalize that the programme is running.
        digitalWrite(LED, HIGH);
        delay(50);
        digitalWrite(LED, LOW);
        
        // Add delay to slow down ADC sampling
        delay(50);
        vccSense = readAverageAnalogSignal(VCC_ADC);
        gndSense = readAverageAnalogSignal(GND_ADC);
        switch1 = readAverageAnalogSignal(SWITCH_1_ADC);
        switch2 = readAverageAnalogSignal(SWITCH_2_ADC);
        throttleSense = readThrottleSense();
        
        vccStatusOk = evaluateVccSense(vccSense);
        gndStatusOk = evaluateGndSense(gndSense);
        switchStatus1 = evaluateSwitch(switch1);
        switchStatus2 = evaluateSwitch(switch2);
        throttlePressed = evaluateThrottleSense(throttleSense);
        
        ignitionSense = readIgnitionSense(); //for stopping while loop
        ignitionOn = evaluateIgnitionSense(ignitionSense); //to stopping while-loop
        
        if(vccStatusOk == false){
          reportError("Error: VCC");
          epbState = EPB_ERROR;
        }
        
        if(gndStatusOk == false){
          reportError("Error: GND");
          epbState = EPB_ERROR;
        }

        if (ignitionOn == false) {
          epbState = IGNITION_OFF;
        }
        
        if((vccStatusOk == true) && (gndStatusOk == true)){
          // Reasons Brake-/Switch/Circuit-State based on measured ADC-values and defined voltage intervals.
          sysState = analyseSystemState(switchStatus1, switchStatus2, switch1, switch2);
        }

        boolean slaveResultsOk = checkSlaveResults(sysState);
    
        if(!slaveResultsOk){
          reportError("Error: Slave-Results!");
          //noError = false;
          epbState = EPB_ERROR;
        }

        //!!!Achtung gndOK und vccOK noch mit berücksichtigen
        switch (sysState){
          case NOTHING_PRESSED:
            // If driver starts to move vehicle, release brake automatically
            if(throttlePressed){
                releaseBrake();
                
                if (emergencyBrakeApplied == true) {
                    emergencyBrakeApplied = false;
                }
            }
            // 
            break;
            
          case APPLY_PRESSED:
            if(!throttlePressed){
              applyBrake();
            }
            else {
              Serial.println("#DANGER# Trying to apply Brake vehicle is in motion!");
              
              if ((count == 0) && (emergencyBrakeApplied == false)) {
                passedTime = 0;
                startTime = millis();
              } else if ((count > 0) && (emergencyBrakeApplied == false)){
                passedTime = millis() - startTime;
                
              }
              
              if ((passedTime >= 2500) && (emergencyBrakeApplied == false)) {
                 Serial.println("## EMERGENCY BRAKE APPLIED ##");
                 emergencyBrakeApplied = true;
                 applyBrake();
                 count = 0;
                 oldCount = 0;
              } else {        
                count++;
              }        
            }
            break;
          
          case RELEASE_PRESSED:
            releaseBrake();
            if (emergencyBrakeApplied == true) {
                emergencyBrakeApplied = false;
            }
            break;
          
          case SHORT_VCC_C:
            reportError("Short VCC -> C.");
            noError = false;
            epbState = EPB_ERROR;
            break;
          
          case SHORT_VCC_E:
            reportError("Short VCC -> E.");
            noError = false;
            epbState = EPB_ERROR;
            break;
          
          case SHORT_GND_D:
            reportError("Short GND -> D.");
            noError = false;
            epbState = EPB_ERROR;
            break;
          
          case SHORT_GND_F:
            reportError("Short GND -> F.");
            noError = false;
            epbState = EPB_ERROR;
            break;
          
          case OPEN_RES_G:
            reportError("Open res @ G.");
            noError = false;
            epbState = EPB_ERROR;
            break;
          
          case OPEN_RES_H:
            reportError("Open res @ H.");
            noError = false;
            epbState = EPB_ERROR;
            break;
          
          case UNSYNC_SWITCH_BEHAVE:
            reportError("+++ Unsynchronised switch behaviour! +++");
            //ignore
            break;
          
          default:
            reportError("Unknown System State!");
            noError = false;
            epbState = EPB_ERROR;
          break;   
        }
    } else if (epbState == IGNITION_OFF) {
        // If ignition turns off, wait for ignition to 
        // turn on again
        ledsOff();
        epbState = IGNITION_OFF_WAITING;
        Serial.println("Ignition off");
    } else if (epbState == IGNITION_OFF_WAITING) {
        ignitionSense = readIgnitionSense();
        ignitionOn = evaluateIgnitionSense(ignitionSense);
        
        if (ignitionOn == true) {
            epbState = IGNITION_ON_INIT;
        }
        
    } else if (epbState == EPB_ERROR) {
        // If the ignition is off or there is an error, we end up here
        // Turn on error LED
        brakeErrorLedOn();
        
        // Check ignition-state,  because we are not in the while-loop anymore
        ignitionSense = readIgnitionSense();
        ignitionOn = evaluateIgnitionSense(ignitionSense);
        
        if (ignitionOn == true) {
            epbState = EPB_ERROR_IGN_ON_2;
        } else {
            epbState = EPB_ERROR_IGN_OFF;
        }
    } else if (epbState == EPB_ERROR_IGN_ON_1) {
        // This blinks the dashboard LEDs to simulate turning on the ignition
        // as in the "IGNITION_ON_INIT" state
        ledsForceOn();
        delay(650);
        ledsForceOff();
        ledsOn();
        epbState = EPB_ERROR_IGN_ON_2;
    } else if (epbState == EPB_ERROR_IGN_ON_2) {
        // Here we just wait for the ignition to turn off since the brakes
        // don't work anymore.
        // Check ignition-state,  because we are not in the while-loop anymore
        ignitionSense = readIgnitionSense();
        ignitionOn = evaluateIgnitionSense(ignitionSense);
        
        if (ignitionOn == false) {
            // Short delay to debounce switch
            delay(50);
            epbState = EPB_ERROR_IGN_OFF;
        }
    } else if (epbState == EPB_ERROR_IGN_OFF) {
        // Turn off dashboard lights
        ledsOff();
        
        // Check ignition-state,  because we are not in the while-loop anymore
        ignitionSense = readIgnitionSense();
        ignitionOn = evaluateIgnitionSense(ignitionSense);
        // Check "in motion" sensor as this is used to "repair" the brakes when
        // ignition is off
        throttleSense = readThrottleSense();
        throttlePressed = evaluateThrottleSense(throttleSense);
    
        // If ignition is off and throtte sense is true, clear errors
        // otherwises turn LEDs on and off with ignition switch status
        if (throttlePressed == true) {
            repairBrakes();
            epbState = IGNITION_OFF_WAITING;
        } else if (ignitionOn == true) {
            // If ignition is on again, cycle through loop again in error state
            epbState = EPB_ERROR_IGN_ON_1;
        }
    }
    
    // Simulate the parking break position with a servo
    brakeStatus = getBrakeStatus();
    servoPos = updateServoPos(brakeStatus);
    
    // check value, before passing it to a library function! MISRA-Dir. 4.11
    if((servoPos > CLOSED_LIMIT) && (servoPos < OPEN_LIMIT)) { 
      // If servo is detached, reattach it
      if (brakeSimulator.attached() == false) {
        brakeSimulator.attach(SERVO_PIN);
      }
      brakeSimulator.write(servoPos);
    }  
#if 0    
    // To stop servo "crunching" we disconnect it when limit it reached
    if((servoPos == CLOSED_LIMIT) || (servoPos == OPEN_LIMIT)) {
      if (brakeSimulator.attached() == true) {
        brakeSimulator.detach();
      }
    }
#endif
 
 } // end of loop()
