/*******************************************************************************
* Project: EPB - Electric Parking Brake
*
* iSYSTEM AG, Markus Huber and Stuart Cording
*
*******************************************************************************/

/******************************************************************************/
/** \file constants.h
* 
* This module defines a range of constant values used by the EPB Project. These
* range from input/output pin definitions for the Arduino board, through to
* upper and lower limits for ADC results during evaluation.
*
*******************************************************************************/

/*******************************************************************************
*
*                            EPB - CONSTANTS MODULE
*
*******************************************************************************/
#ifndef Constants_h
#define Constants_h

/******************************************************************************/
/** \defgroup EpbProjectConstants Constants used in EPB Project
* Contains many constant values for hardware pin definition, limits for ADC
* evaluation as well as enumerated types for state machines.
* @{ 
*******************************************************************************/

/*******************************************************************************
*                                 INCLUDE FILES
*******************************************************************************/
#include "Arduino.h"


/*******************************************************************************
*                                    EXTERNS
*******************************************************************************/


/*******************************************************************************
*                             DEFAULT CONFIGURATION
*******************************************************************************/


/*******************************************************************************
*                               DEFINES AND MACROS
*******************************************************************************/
/******************************************************************************/
/** \def SERVO_PIN
* Defines the Arduino PWM-capable pin to be used for controlling the servo that
* simulates the brake.
*******************************************************************************/
#define SERVO_PIN 7

/******************************************************************************/
/** \def LED
* Defines the digital pin associated with the Arduino's on-board green LED
*******************************************************************************/
#define LED 13

/******************************************************************************/
/** \def ERROR_LED
* Defines the digital pin associated with the red "Brake Error" LED
*******************************************************************************/
#define ERROR_LED 12

/******************************************************************************/
/** \def INFO_LED
* Defines the digital pin associated with the orange "Brake State" LED
*******************************************************************************/
#define INFO_LED 11

/******************************************************************************/
/** \def IGNITION_PIN
* Defines the digital pin associated with the ignition detection input
*******************************************************************************/
#define IGNITION_PIN 8

/******************************************************************************/
/** \def THROTTLE_PIN
* Defines the digital pin associated with the "in motion" switch input
*******************************************************************************/
#define THROTTLE_PIN 9

/******************************************************************************/
/** \def VCC_ADC
* Defines the analogue pin associated with the measurement of the VCC sense line
*******************************************************************************/
#define VCC_ADC A3

/******************************************************************************/
/** \def GND_ADC
* Defines the analogue pin associated with the measurement of the GND sense line
*******************************************************************************/
#define GND_ADC A0

/******************************************************************************/
/** \def SWITCH_1_ADC
* Defines the analogue pin associated with the measurement of contact 1 of 
* brake rocker switch
*******************************************************************************/
#define SWITCH_1_ADC A6

/******************************************************************************/
/** \def SWITCH_2_ADC
* Defines the analogue pin associated with the measurement of contact 2 of 
* brake rocker switch
*******************************************************************************/
#define SWITCH_2_ADC A8

/******************************************************************************/
/** \def ADC_READ_RESOLUTION
* Defines the resolution of the ADC in bits
*******************************************************************************/
#define ADC_READ_RESOLUTION 12

/******************************************************************************/
/** \def ADC_MAX_VALUE
* Defines the maximum result that can be expected from the ADC in decimal
*******************************************************************************/
#define ADC_MAX_VALUE (pow(2, ADC_READ_RESOLUTION) - 1)

/******************************************************************************/
/** \def SUPPLY_VOLTAGE
* Defines the system supply voltage (3.3V for Cortex-based MCUs)
*******************************************************************************/
#define SUPPLY_VOLTAGE 3.3

/* Limits for ADC values when evaluating switch results and Vcc/GND sense lines */
/******************************************************************************/
/** \def SWITCH_VALUE_MINIMUM
* Defines minumum value that a switch input can have from ADC result
*******************************************************************************/
#define SWITCH_VALUE_MINIMUM            0

/******************************************************************************/
/** \def SWITCH_VALUE_GROUND_LIMIT
* Defines range above #SWITCH_VALUE_MINIMUM within which it is assumed switch 
* input is shorted to ground
*******************************************************************************/
#define SWITCH_VALUE_GROUND_LIMIT       SWITCH_VALUE_MINIMUM + 25

/******************************************************************************/
/** \def SWITCH_VALUE_MAXIMUM
* Defines maximum value that a switch input can have from ADC result
*******************************************************************************/
#define SWITCH_VALUE_MAXIMUM            ADC_MAX_VALUE

/******************************************************************************/
/** \def SWITCH_VALUE_VCC_LIMIT
* Defines range below #SWITCH_VALUE_MAXIMUM within which it is assumed switch 
* input is shorted to VCC
*******************************************************************************/
#define SWITCH_VALUE_VCC_LIMIT          SWITCH_VALUE_MAXIMUM - 25

/******************************************************************************/
/** \def VCC_SENSE_LIMIT
* Defines lower limit value that can be considered "VCC good"
*******************************************************************************/
#define VCC_SENSE_LIMIT                 ADC_MAX_VALUE - 25

/******************************************************************************/
/** \def GND_SENSE_LIMIT
* Defines upper limit value that can be considered "GND good"
*******************************************************************************/
#define GND_SENSE_LIMIT                 0 + 25

/******************************************************************************/
/** \def SWITCH_VALUE_NORMAL
* Defines average ADC value that we expect from a switch input in its unpressed
* state
*******************************************************************************/
#define SWITCH_VALUE_NORMAL             1370

/******************************************************************************/
/** \def SWITCH_VALUE_NORMAL_DEVIATION
* Defines deviation from average ADC value that we expect from a switch input in
* its unpressed state
*******************************************************************************/
#define SWITCH_VALUE_NORMAL_DEVIATION   150

/******************************************************************************/
/** \def SWITCH_VALUE_NORMAL_UPPER
* Defines ADC value upper limit for switch input that can still be considered 
* "good" in its unpressed state
*******************************************************************************/
#define SWITCH_VALUE_NORMAL_UPPER       SWITCH_VALUE_NORMAL + SWITCH_VALUE_NORMAL_DEVIATION

/******************************************************************************/
/** \def SWITCH_VALUE_NORMAL_LOWER
* Defines ADC value lower limit for switch input that can still be considered 
* "good" in its unpressed state
*******************************************************************************/
#define SWITCH_VALUE_NORMAL_LOWER       SWITCH_VALUE_NORMAL - SWITCH_VALUE_NORMAL_DEVIATION

/******************************************************************************/
/** \def SWITCH_VALUE_LOW
* Defines average lower ADC value that we expect from a switch input in its 
* pressed state
* \note When switch is pressed, one ADC input drops to around #SWITCH_VALUE_LOW
* whilst the other ADC input will rise to around #SWITCH_VALUE_HIGH
*******************************************************************************/
#define SWITCH_VALUE_LOW                480

/******************************************************************************/
/** \def SWITCH_VALUE_LOW_DEVIATION
* Defines deviation from lower average ADC value that we expect from a switch 
* input in its pressed state
*******************************************************************************/
#define SWITCH_VALUE_LOW_DEVIATION      150

/******************************************************************************/
/** \def SWITCH_VALUE_LOW_UPPER
* Defines ADC value upper limit for switch input that can still be considered 
* "good" in its pressed state around #SWITCH_VALUE_LOW
*******************************************************************************/
#define SWITCH_VALUE_LOW_UPPER          SWITCH_VALUE_LOW + SWITCH_VALUE_LOW_DEVIATION

/******************************************************************************/
/** \def SWITCH_VALUE_LOW_LOWER
* Defines ADC value lower limit for switch input that can still be considered 
* "good" in its pressed state around #SWITCH_VALUE_LOW
*******************************************************************************/
#define SWITCH_VALUE_LOW_LOWER          SWITCH_VALUE_LOW - SWITCH_VALUE_LOW_DEVIATION

/******************************************************************************/
/** \def SWITCH_VALUE_HIGH
* Defines average upper ADC value that we expect from a switch input in its 
* pressed state
* \note When switch is pressed, one ADC input drops to around #SWITCH_VALUE_LOW
* whilst the other ADC input will rise to around #SWITCH_VALUE_HIGH
*******************************************************************************/
#define SWITCH_VALUE_HIGH               3155

/******************************************************************************/
/** \def SWITCH_VALUE_HIGH_DEVIATION
* Defines deviation from lower average ADC value that we expect from a switch 
* input in its pressed state
*******************************************************************************/
#define SWITCH_VALUE_HIGH_DEVIATION     150

/******************************************************************************/
/** \def SWITCH_VALUE_HIGH_UPPER
* Defines ADC value upper limit for switch input that can still be considered 
* "good" in its pressed state around #SWITCH_VALUE_HIGH
*******************************************************************************/
#define SWITCH_VALUE_HIGH_UPPER         SWITCH_VALUE_HIGH + SWITCH_VALUE_HIGH_DEVIATION

/******************************************************************************/
/** \def SWITCH_VALUE_HIGH_LOWER
* Defines ADC value lower limit for switch input that can still be considered 
* "good" in its pressed state around #SWITCH_VALUE_HIGH
*******************************************************************************/
#define SWITCH_VALUE_HIGH_LOWER         SWITCH_VALUE_HIGH - SWITCH_VALUE_HIGH_DEVIATION

/******************************************************************************/
/** \def SWITCH_VALUE_OPENCCT
* Defines average ADC value that we expect from a switch input if it were to go
* open circuit. Since the ADC input is hanging between VCC and GND via two 22k
* resistors, the expected value is half the #SWITCH_VALUE_MAXIMUM
*******************************************************************************/
#define SWITCH_VALUE_OPENCCT            SWITCH_VALUE_MAXIMUM / 2

/******************************************************************************/
/** \def SWITCH_VALUE_OPENCCT_DEVIATION
* Defines deviation from average ADC value that we expect from a switch input
* that has gone open circuit
*******************************************************************************/
#define SWITCH_VALUE_OPENCCT_DEVIATION  150

/******************************************************************************/
/** \def SWITCH_VALUE_OPENCCT_UPPER
* Defines ADC value upper limit for switch input that can still be considered 
* "open circuit"
*******************************************************************************/
#define SWITCH_VALUE_OPENCCT_UPPER      SWITCH_VALUE_OPENCCT + SWITCH_VALUE_OPENCCT_DEVIATION

/******************************************************************************/
/** \def SWITCH_VALUE_OPENCCT_LOWER
* Defines ADC value lower limit for switch input that can still be considered 
* "open circuit"
*******************************************************************************/
#define SWITCH_VALUE_OPENCCT_LOWER      SWITCH_VALUE_OPENCCT - SWITCH_VALUE_OPENCCT_DEVIATION


/*******************************************************************************
*                                   DATA TYPES
*******************************************************************************/

/* FAIL_IGNORE is only necessary if you want to double-check the detected HW-Failure.
   See notes and version sketch_mar30a for more information.

*/

/******************************************************************************/
/** \enum voltInterval
* Definition of possible states to describe the voltage measured via the ADC.
*******************************************************************************/
enum voltInterval {
    UNDEFINED_INTERVAL,     /**< Signal is in an undefined region */
    FAIL_GND_SHORT,         /**< Signal is shorted to ground */
    FAIL_VCC_SHORT,         /**< Signal is shorted to VCC */
    FAIL_OPEN,              /**< Signal is open circuit */
    SWITCH_NORM_VAL,        /**< Signal is in its unpressed state */
    SWITCH_LOW_VAL,         /**< Signal is in its pressed lower state */
    SWITCH_HIGH_VAL,        /**< Signal is in its pressed upper state */
    FAIL_IGNORE             /**< TBD */
};

/******************************************************************************/
/** \enum systemState
* Definition of possible states to describe status of the rocker switch.
*******************************************************************************/
enum systemState { 
    UNDEFINED_STATE = 7,    /**< State of switch is undefined */
    UNSYNC_SWITCH_BEHAVE,   /**< Switch is in 'half-pressed' state */
    NOTHING_PRESSED,        /**< Switch is in resting state (unpressed) */
    APPLY_PRESSED,          /**< Switch is in "apply" state */
    RELEASE_PRESSED,        /**< Switch is in "release" state */
    SHORT_VCC_C,            /**< Short to VCC in cable loom */
    SHORT_VCC_E,            /**< Short to VCC in cable loom */
    SHORT_GND_D,            /**< Short to GND in cable loom */
    SHORT_GND_F,            /**< Short to GND in cable loom */
    OPEN_RES_G,             /**< Open circuit in cable loom */
    OPEN_RES_H              /**< Open circuit in cable loom */
};


/*******************************************************************************
*                                GLOBAL VARIABLES
*******************************************************************************/


/*******************************************************************************
*                              FUNCTION PROTOTYPES
*******************************************************************************/


/*******************************************************************************
*                              CONFIGURATION ERRORS
*******************************************************************************/


/*******************************************************************************
*
*                             EPB - BRAKE MODULE END
*
*******************************************************************************/

/**
* @} 
*******************************************************************************/

#endif
