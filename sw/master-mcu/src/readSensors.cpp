#include "readSensors.h"

// Read Values at ADCs and converts into Volt.
float readVccSense(){
  return analogRead(VCC_ADC) * (SUPPLY_VOLTAGE / ADC_MAX_VALUE);
}

float readGndSense(){
  return analogRead(GND_ADC)*(SUPPLY_VOLTAGE / ADC_MAX_VALUE);
}

float readSwitchSense1(){
  return analogRead(SWITCH_1_ADC) * (SUPPLY_VOLTAGE / ADC_MAX_VALUE);
}

float readSwitchSense2(){
  return analogRead(SWITCH_2_ADC) * (SUPPLY_VOLTAGE / ADC_MAX_VALUE);
}

// Read Values of ADCs - returns average of 8 samples.
unsigned int readAverageAnalogSignal(unsigned int channel) {
    unsigned int readValue = 0;
    unsigned int count = 0;
    
    for (count = 0; count < 8; ++count) {
        readValue += analogRead(channel);
    }
    
    readValue = readValue / 8;
    
  return readValue;
}

// Read Values at ADCs - returns integer.
unsigned int readVccSenseInt(){
  return analogRead(VCC_ADC);
}

unsigned int readGndSenseInt(){
  return analogRead(GND_ADC);
}

unsigned int readSwitchSense1Int(){
  return analogRead(SWITCH_1_ADC);
}

unsigned int readSwitchSense2Int(){
  return analogRead(SWITCH_2_ADC);
}

int readIgnitionSense(){
  return digitalRead(IGNITION_PIN);
}

int readThrottleSense(){
  return digitalRead(THROTTLE_PIN);
}
