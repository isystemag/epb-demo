#ifndef Led_h
#define Led_h

void ledsOn(void);
void ledsOff(void);
void ledsForceOn(void);
void ledsForceOff(void);
void initBrakeStatusLed(int led);
void initBrakeErrorLed(int led);
void brakeStatusLedOn(void);
void brakeStatusLedOff(void);
void brakeErrorLedOn(void);
void brakeErrorLedOff(void);

#endif