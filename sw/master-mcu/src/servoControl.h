#ifndef ServoControl_h
#define ServoControl_h

#include "Arduino.h"
#include "brake.h"
#include "constants.h"

#define CLOSED_LIMIT    104
#define OPEN_LIMIT      144
#define STEP_VALUE      4

/*
  Calculates the Position of the servo according to if the brake is being applied or released.
  return: new Servoposition
*/
int updateServoPos(byte brakeState);

#endif
