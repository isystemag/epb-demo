/*******************************************************************************
* Project: EPB - Electric Parking Brake
*
* iSYSTEM AG, Markus Huber and Stuart Cording
*
*******************************************************************************/


/******************************************************************************/
/** \file brake.cpp
* 
* This module implements the simulation of a brake for the EPB project. The main
* code uses this to control the servo that simulates the brake.
*
*******************************************************************************/


/*******************************************************************************
*
*                               EPB - BRAKE MODULE
*
*******************************************************************************/


/******************************************************************************/
/** \addtogroup BrakeModuleFunction
* @{ 
*******************************************************************************/


/*******************************************************************************
*                                 INCLUDE FILES
*******************************************************************************/
#include "Arduino.h"
#include "brake.h"
#include "constants.h"
#include "led.h"


/*******************************************************************************
*                                 LOCAL DEFINES
*******************************************************************************/


/*******************************************************************************
*                                LOCAL CONSTANTS
*******************************************************************************/


/*******************************************************************************
*                                LOCAL DATA TYPES
*******************************************************************************/


/*******************************************************************************
*                                  LOCAL TABLES
*******************************************************************************/


/*******************************************************************************
*                             LOCAL GLOBAL VARIABLES
*******************************************************************************/
/******************************************************************************/
/** \var static byte brakeState 
* Holds state of the brake for the Brake Module. Brake state starts as 
* #BRAKE_RELEASED, as it must be open to be fitted to a vehicle i.e. this is the 
* post-manufacture delivery state.
*******************************************************************************/
static byte brakeState = BRAKE_RELEASED;


/*******************************************************************************
*                             LOCAL FUNCTION PROTOTYPES
*******************************************************************************/


/*******************************************************************************
*                            LOCAL CONFIGURATION ERRORS
*******************************************************************************/


byte getBrakeStatus() {
  return brakeState;
}

void applyBrake() {
    // Note that the brake is now in "applied" state and set brake LED to ON
    brakeState = BRAKE_APPLIED;
    // control servo
    //Serial.println("BRAKE_APPLIED");
    brakeStatusLedOn();
}

void releaseBrake() {
    // Note that the brake is now in "released" state and set brake LED to OFF
    brakeState = BRAKE_RELEASED;
    //  control servo
    //Serial.println("BRAKE_RELEASED");
    brakeStatusLedOff();
}

boolean brakeInit() {
    // Initialisation of Brake Module
    //Serial.println("Brake-Initialization");
    
    // Show that brake LED is working
    brakeStatusLedOn();
    ledsOn();
    //delay(1000);
    ledsOff();
    brakeStatusLedOff();
    
    // Initialise state of brake
    if (brakeState == BRAKE_APPLIED){
        applyBrake();
    } else if (brakeState == BRAKE_RELEASED){
        releaseBrake();
    }

    // Always returns true upon completion
    return true;
}

/*******************************************************************************
*
*                             EPB - BRAKE MODULE END
*
*******************************************************************************/

/**
* @} 
*******************************************************************************/