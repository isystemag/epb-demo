env:
  useQualifiedFuncName: false
  autoConnect: false
  autoIdFormat: ${_uid}
  defaultRetValName: rv
  initBeforeRun: true
  disableInterrupts: false
  breakpointType: keepWinIDEASetting
  initSequence:
  - action: download
  - action: run
    params:
    - loop
  - action: delAllBreakpoints
  scriptConfig:
    timeout: 100
  evaluatorConfig:
    isOverrideWinIDEASettings: true
    charDisplay: Both
    isAnsiChar: true
    isHex: true
    binaryDisplay: Blanks
    isDisplayPointerMemArea: true
    isCharArrayAsString: true
    isDereferenceStringPointers: true
    addressDisplay: HexPrefix
    enumDisplay: Both
    isDisplayCollapsedArrayStruct: true
    vagueFloatPrecision: 1e-5
  testCaseTargetInit:
    downloadOnTCInit: false
    resetOnTCInit: false
    runOnTCInit: false
  checkTargetState: true
  verifySymbolsBeforeRun: false
reportConfig:
  winIDEAVersion: 9.12.284
testCases:
  tests:
  - id: vw788qss5zds
    desc: |-
      Tests for case where input switch is not pressed with average ADC value.
    func:
      func: evaluateSwitch
      params:
      - 1370
    assert:
      expressions:
      - _isys_rv == SWITCH_NORM_VAL
    analyzer:
      runMode: start
      document: master-mcu-evaluation-module-${_function}-${_testId}.trd
      openMode: w
      isSlowRun: true
      isSaveAfterTest: true
      isCloseAfterTest: true
      coverage:
        isActive: true
        isIgnoreNonReachableCode: true
        isAssemblerInfo: true
        isExportModuleLines: true
        isExportSources: true
        isExportFunctionLines: true
        isExportAsm: true
        isExportRanges: true
        statistics:
        - func: evaluateSwitch
    tests:
    - id: vw78hhn3w1s0
      imports:
        desc:
          inherit: false
        analyzer:
          inherit: true
      desc: |-
        Tests for case where input switch is not pressed with upper limit ADC value.
      func:
        func: evaluateSwitch
        params:
        - 1520
      assert:
        expressions:
        - _isys_rv == SWITCH_NORM_VAL
    - id: vw78hlegckzk
      imports:
        desc:
          inherit: false
        analyzer:
          inherit: true
      desc: |-
        Tests for case where input switch is not pressed with lower limit ADC value.
      func:
        func: evaluateSwitch
        params:
        - 1220
      assert:
        expressions:
        - _isys_rv == SWITCH_NORM_VAL
    - id: vw78hqg2c3gg
      imports:
        desc:
          inherit: false
      desc: |-
        Tests for case where input switch is not pressed and ADC value lies just above upper limit for "not pressed".
      func:
        func: evaluateSwitch
        params:
        - 1521
      assert:
        expressions:
        - _isys_rv == UNDEFINED_INTERVAL
    - id: vw78i5jynvwg
      imports:
        desc:
          inherit: false
        analyzer:
          inherit: false
      desc: |-
        Tests for case where input switch is not pressed and ADC value lies just below lower limit for "not pressed".
      func:
        func: evaluateSwitch
        params:
        - 1219
      assert:
        expressions:
        - _isys_rv == UNDEFINED_INTERVAL
      analyzer:
        runMode: start
        document: master-mcu-evaluation-module-${_function}-${_testId}.trd
        openMode: w
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          mergeScope: siblingsAndParent
          exportFormat: HTML
          exportFile: master-mcu-evaluation-module-${_function}.html
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
