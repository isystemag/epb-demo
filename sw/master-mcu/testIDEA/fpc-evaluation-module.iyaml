env:
  useQualifiedFuncName: false
  autoConnect: false
  autoIdFormat: ${_uid}
  defaultRetValName: rv
  initBeforeRun: true
  disableInterrupts: false
  breakpointType: keepWinIDEASetting
  initSequence:
  - action: download
  - action: run
    params:
    - loop
  - action: delAllBreakpoints
  scriptConfig:
    timeout: 100
  evaluatorConfig:
    isOverrideWinIDEASettings: true
    charDisplay: Both
    isAnsiChar: true
    isHex: true
    binaryDisplay: Blanks
    isDisplayPointerMemArea: true
    isCharArrayAsString: true
    isDereferenceStringPointers: true
    addressDisplay: HexPrefix
    enumDisplay: Both
    isDisplayCollapsedArrayStruct: true
    vagueFloatPrecision: 1e-5
  testCaseTargetInit:
    downloadOnTCInit: false
    resetOnTCInit: false
    runOnTCInit: false
  checkTargetState: true
  verifySymbolsBeforeRun: false
reportConfig:
  winIDEAVersion: 9.12.288
  outFormat: yaml
  fileName: C:\Users\Stuart\sw-dev\epb-demo\sw\master-mcu\testResults\fpc-evaluation-module-coverage.xml
  csvSeparator: ','
testCases:
  tests:
  - id: EvaluationModule
    desc: |-
      Test values just outside boundary for Low position detected by ADC
    func:
      func: evaluateSwitch
      params:
      - 631
    assert:
      expressions:
      - _isys_rv == UNDEFINED_INTERVAL
    analyzer:
      runMode: start
      document: eval_mod_${_function}_${_testId}.trd
      isSlowRun: true
      isSaveAfterTest: true
      isCloseAfterTest: true
      coverage:
        isActive: true
        isIgnoreNonReachableCode: true
        isAssemblerInfo: true
        isLaunchViewer: true
        isExportModuleLines: true
        isExportSources: true
        isExportFunctionLines: true
        isExportAsm: true
        isExportRanges: true
        statistics:
        - func: evaluateSwitch
    tests:
    - id: EvaluationModule0002
      desc: |-
        Test values just outside boundary for Low position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 329
      assert:
        expressions:
        - _isys_rv == UNDEFINED_INTERVAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0003
      desc: |-
        Test values just outside boundary for High position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 3306
      assert:
        expressions:
        - _isys_rv == UNDEFINED_INTERVAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0004
      desc: |-
        Test values just outside boundary for High position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 3004
      assert:
        expressions:
        - _isys_rv == UNDEFINED_INTERVAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0005
      desc: |-
        Test values just outside boundary for Normal position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 1521
      assert:
        expressions:
        - _isys_rv == UNDEFINED_INTERVAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0006
      desc: |-
        Test values just outside boundary for Normal position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 1219
      assert:
        expressions:
        - _isys_rv == UNDEFINED_INTERVAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0007
      desc: |-
        Test a valid value for High position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 3155
      assert:
        expressions:
        - _isys_rv == SWITCH_HIGH_VAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0008
      desc: |-
        Test a valid value for High position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 3005
      assert:
        expressions:
        - _isys_rv == SWITCH_HIGH_VAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0009
      desc: |-
        Test a valid value for High position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 3305
      assert:
        expressions:
        - _isys_rv == SWITCH_HIGH_VAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0010
      desc: |-
        Test a valid value for Low position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 480
      assert:
        expressions:
        - _isys_rv == SWITCH_LOW_VAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0011
      desc: |-
        Test a valid value for Low position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 330
      assert:
        expressions:
        - _isys_rv == SWITCH_LOW_VAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0012
      desc: |-
        Test a valid value for Low position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 630
      assert:
        expressions:
        - _isys_rv == SWITCH_LOW_VAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0013
      desc: |-
        Test a valid value for Normal position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 1220
      assert:
        expressions:
        - _isys_rv == SWITCH_NORM_VAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0014
      desc: |-
        Test a valid value for Normal position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 1370
      assert:
        expressions:
        - _isys_rv == SWITCH_NORM_VAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
    - id: EvaluationModule0015
      desc: |-
        Test a valid value for Normal position detected by ADC
      func:
        func: evaluateSwitch
        params:
        - 1500
      assert:
        expressions:
        - _isys_rv == SWITCH_NORM_VAL
      analyzer:
        runMode: start
        document: eval_mod_${_function}_${_testId}.trd
        isSlowRun: true
        isSaveAfterTest: true
        isCloseAfterTest: true
        coverage:
          isActive: true
          isIgnoreNonReachableCode: true
          mergeScope: siblingsAndParent
          exportFormat: HTML
          exportFile: ${_function}_code_coverage.html
          isAssemblerInfo: true
          isLaunchViewer: true
          isExportModuleLines: true
          isExportSources: true
          isExportFunctionLines: true
          isExportAsm: true
          isExportRanges: true
          statistics:
          - func: evaluateSwitch
