<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
    <xsl:output method="text" encoding="UTF-8"/>
    <xsl:param name="nametag"/>
    <xsl:include href="../template_lib.xsl"/>
    <xsl:template name="formatRequirement">
        <xsl:param name="display"/>
        <xsl:value-of select="Name"/>
        <xsl:if test="$display='long'">
            <xsl:text>
            </xsl:text>
            <xsl:if test="Description">
                <xsl:value-of select="Description"/>
            </xsl:if>
            <xsl:if test="LastModified or URL or Source or BaseLine">
                <xsl:text>
                </xsl:text>
                <xsl:text><![CDATA[
                (]]></xsl:text>
                <xsl:if test="LastModified">
                    <xsl:text><![CDATA[Last modified: ]]></xsl:text>
                    <xsl:value-of select="LastModified"/>
                </xsl:if>
                <xsl:if test="URL">
                    <xsl:text><![CDATA[URL: ]]></xsl:text>
                    <xsl:value-of select="URL"/>
                </xsl:if>
                <xsl:if test="Source">
                    <xsl:text><![CDATA[Source: ]]></xsl:text>
                    <xsl:value-of select="Source"/>
                </xsl:if>
                <xsl:if test="BaseLine">
                    <xsl:text><![CDATA[Baseline: ]]></xsl:text>
                    <xsl:value-of select="BaseLine"/>
                </xsl:if>
                <xsl:text><![CDATA[)
            ]]></xsl:text>
            </xsl:if>
        </xsl:if>
    </xsl:template>
    <xsl:template match="/">
        <xsl:result-document href="{$nametag}.py">
            <xsl:text><![CDATA[# This script was generated with SeppMed MBTsuite tool. Do not modify it manually!]]>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[from __future__ import print_function]]>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
            <xsl:text><![CDATA[import sys]]>&#13;&#10;</xsl:text>
            <xsl:text><![CDATA[sys.path.append('../../nanoHIL')]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[import time]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[import isystem.connect as ic]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[import teststatus as ts]]>&#13;&#10;</xsl:text>
            <xsl:text><![CDATA[import nanohil as nh]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[from isystem.connect import IConnectDebug]]>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>

            <xsl:for-each select="/TestCases/TestCase">
                <xsl:sort select="Name"/>
                <xsl:variable name="TCDesc">
                    <xsl:apply-templates select="." mode="TCDescription"/>
                    <!--<xsl:value-of select="string-join(TestCaseElement[TCDescription]/TCDescription/Line, ' ')"/>-->
                </xsl:variable>
                <xsl:text>&#13;&#10;<![CDATA[#]]></xsl:text>
                <xsl:value-of select="Name"/>
                <xsl:text><![CDATA[    - TestCase_]]></xsl:text>
                <xsl:number value="position()"/>
                <xsl:if test="$TCDesc">
                    <xsl:text>&#13;&#10;<![CDATA[# ]]></xsl:text>
                    <xsl:value-of select="$TCDesc"/>
                </xsl:if>
                <xsl:text>&#13;&#10;<![CDATA[def ]]></xsl:text>
                <xsl:value-of select="Name"/>
                <xsl:text><![CDATA[(results):]]>&#13;&#10;</xsl:text>
				<xsl:text><![CDATA[    print("]]></xsl:text>
                <xsl:value-of select="Name"/>
                <xsl:text><![CDATA[ - TestCase_]]></xsl:text>
                <xsl:number value="position()"/>
                <xsl:text><![CDATA[")]]>&#13;&#10;</xsl:text>
				<xsl:text><![CDATA[    key = ']]></xsl:text>
                <xsl:value-of select="Name"/>
                <xsl:text><![CDATA[']]>&#13;&#10;</xsl:text>
				<xsl:text><![CDATA[    results[key] = []]]>&#13;&#10;</xsl:text>
                <xsl:text><![CDATA[    testStatus = "PASS"]]>&#13;&#10;</xsl:text>
				<xsl:text>&#13;&#10;</xsl:text>
				<xsl:text><![CDATA[    try:]]></xsl:text>  
				<xsl:for-each select="TestCaseElement">
					<xsl:for-each select="TCDescription/Line">
						<xsl:if test="normalize-space(.)">
							<xsl:text>&#13;&#10;</xsl:text>
							<xsl:text><![CDATA[			#]]></xsl:text>
							<xsl:value-of select="."/>							
						</xsl:if>
					</xsl:for-each>
                
                    <xsl:for-each select="Code/Line">
						<xsl:text>&#13;&#10;</xsl:text>
                        <xsl:text><![CDATA[        ]]></xsl:text>
                        <xsl:value-of select="." disable-output-escaping="yes"/>
                    </xsl:for-each>
					<xsl:if test="TCDescription/Line or Code/Line">
						<xsl:text>&#13;&#10;</xsl:text>
					</xsl:if>
                </xsl:for-each>
                <xsl:text>&#13;&#10;</xsl:text>
				<xsl:text><![CDATA[    except Exception as ex:]]>&#13;&#10;</xsl:text>
                <xsl:text><![CDATA[        testStatus = "FAIL"]]>&#13;&#10;</xsl:text>
				<xsl:text><![CDATA[        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_ERROR, "]]></xsl:text>
				<xsl:value-of select="Name"/>
                <xsl:text><![CDATA[(): " + str(ex)))]]>&#13;&#10;</xsl:text>
				<xsl:text>&#13;&#10;</xsl:text>
                <xsl:text><![CDATA[    if testStatus == "PASS":]]>&#13;&#10;</xsl:text>
                <xsl:text><![CDATA[        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_OK, "]]></xsl:text>
                <xsl:value-of select="Name"/>
                <xsl:text><![CDATA[(): " + "!PASSED!"))]]>&#13;&#10;</xsl:text>
                <xsl:text><![CDATA[    else:]]>&#13;&#10;</xsl:text>
                <xsl:text><![CDATA[        results[key].append(ts.TestStatus(ts.TestStatus.STATUS_FAIL, "]]></xsl:text>
                <xsl:value-of select="Name"/>
                <xsl:text><![CDATA[(): " + "#FAILED#"))]]>&#13;&#10;</xsl:text>
                <xsl:text>&#13;&#10;</xsl:text>
                <xsl:text>&#13;&#10;</xsl:text>
                <xsl:text><![CDATA[    print("")]]></xsl:text>
                <xsl:text>&#13;&#10;</xsl:text>
				<xsl:text><![CDATA[    return]]>&#13;&#10;</xsl:text>
				<xsl:text>&#13;&#10;</xsl:text>
            </xsl:for-each>

			<xsl:text>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[def executeAllTests():]]>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    results = {}]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    nh.initSystem()]]>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
            <xsl:text><![CDATA[    # Initialise HIL system ]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    #nh.initSystem()]]>&#13;&#10;</xsl:text>
            <xsl:text><![CDATA[    #nh.initNanoHil()]]>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    # Download code ]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    #try:]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    #    dbgCtrl.download() # downloads appplication to target]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    #    dbgCtrl.run()]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    #    #dbgCtrl.waitUntilStopped()]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    #except Exception as ex:]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    #    print(ex)]]>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
            <xsl:text><![CDATA[    waitTime = nh.initStartDelay()]]>&#13;&#10;</xsl:text>
            <xsl:text><![CDATA[    if waitTime > 0:]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[        time.sleep(waitTime)]]>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>        
            
            <xsl:for-each select="/TestCases/TestCase">
                <xsl:sort select="Name"/>
                <xsl:variable name="TCDesc">
                    <xsl:apply-templates select="." mode="TCDescription"/>
                    <!--<xsl:value-of select="string-join(TestCaseElement[TCDescription]/TCDescription/Line, ' ')"/>-->
                </xsl:variable>

                <xsl:text>&#13;&#10;<![CDATA[    ]]></xsl:text>
                <xsl:value-of select="Name"/>
                <xsl:text><![CDATA[(results)]]></xsl:text>                
            </xsl:for-each>
            
			<xsl:text>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    nh.createReport(results)]]>&#13;&#10;</xsl:text>
            
            <xsl:text>&#13;&#10;</xsl:text>
            <xsl:text><![CDATA[    print()]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    nh.getOverallTestStatus()]]>&#13;&#10;</xsl:text>
            
			<xsl:text>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[if __name__ == '__main__':]]>&#13;&#10;</xsl:text>
			<xsl:text><![CDATA[    executeAllTests()]]>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
			<xsl:text>&#13;&#10;</xsl:text>
        </xsl:result-document>
    </xsl:template>
</xsl:stylesheet>
